package com.drahzek.springmvchome;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringMvcHomeApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringMvcHomeApplication.class, args);
	}

}


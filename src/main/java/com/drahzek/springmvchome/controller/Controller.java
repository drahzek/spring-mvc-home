package com.drahzek.springmvchome.controller;

import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.tags.Param;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

@org.springframework.stereotype.Controller
public class Controller {
    @RequestMapping(value = {"/"}, method = RequestMethod.GET)
    public String getHome() {
        return "index.html";
    }

    @RequestMapping(value = "/home", method= RequestMethod.GET)
    public String redirectMethod(@RequestParam (value="name",required = false) String name ){
        return "redirect:" + name;
    }

    @GetMapping("/homes")
    public String testRedirectWithSession(@RequestParam(value="name", required = false) String name,
                                          HttpServletRequest request){
        final HttpSession session = request.getSession();
        if(name != null){
            session.setAttribute("name", name);
            return "redirect:"+name;
        }
        final String redirectFromSession = (String) session.getAttribute("name");
        if(redirectFromSession == null){
            return "redirect:404.html";
        }
        return "redirect:" + redirectFromSession;
    }


    @GetMapping("/holmes")
    public void testRedirectWithSession(@RequestParam(value = "redirect", required = false) String redirect,
                                        HttpServletRequest request, HttpServletResponse response) throws IOException {
        final HttpSession session = request.getSession();

        if(redirect != null){
            session.setAttribute("redirect", redirect);
            response.sendRedirect(redirect);
            return;
        }
        final String redirectFromSession = (String) session.getAttribute("redirect");
        if(redirectFromSession == null){
            response.sendRedirect("404.html");
        }
        else response.sendRedirect(redirectFromSession);
    }


//    @RequestMapping(value = "/dynamic", method=RequestMethod.GET)
//    public String nameMethod (@RequestParam(value="name") String name, Map<String, Object> model) {
//        model.put("name", name);
//        return "dynamic";
//    }

//    @RequestMapping(value = "/dynamic", method=RequestMethod.GET)
//    public String nameMethod (HttpServletRequest request, Map<String, Object> model) {
//        String name = request.getParameter("name");
//        model.put("name", name);
//        return "dynamic";
//    }

    @RequestMapping(value = "/dynamic", method=RequestMethod.GET)
    public String listMultipleParams(HttpServletRequest request, Map<String, Object> model) throws IOException {
        final Map<String, String[]> parameterMap = request.getParameterMap();
        List<Param> params = new ArrayList<>();

        final Set<Map.Entry<String, String[]>> entries = parameterMap.entrySet();
        for(Map.Entry<String, String[]> entry : entries){
            Param p = new Param();
            p.key = entry.getKey();
            p.value = entry.getValue()[0];
            params.add(p);
        }
        model.put("paramsFromReq", params);

        return "dynamic";
    }
}

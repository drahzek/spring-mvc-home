var books = [
    {
        "author": "Douglas Crockford",
        "title": "JavaScript - mocne strony",
        "isbn": "8324619984",
        "publisher": "Helion",
        "editions": [
            {
                "year": "2019",
                "covercolour": "blue"
            },
            {
                "year": "2017",
                "covercolour": "red"
            }
        ]
    },
    {
        "author": "John Resig",
        "title": "Secrets of the JavaScript Ninja",
        "isbn": "9781933988696",
        "publisher": "Manning",
        "editions": [
            {
                "year": "2015",
                "covercolour": "orange"
            },
            {
                "year": "2009",
                "covercolour": "green"
            }
        ]
    },
    {
        "author": "Eric T. Freeman",
        "title": "Head First JavaScript Programming: A Brain-Friendly Guide",
        "isbn": "978-1-4493-4398-9, 9781449343989",
        "publisher": "O'Reilly Media",
        "editions": [
            {
                "year": "2017",
                "covercolour": "gray"
            },
            {
                "year": "2014",
                "covercolour": "blue"
            }
        ]
    },
    {
        "author": "David Flanagan",
        "title": "JavaScript: The Definitive Guide",
        "isbn": "0596805527",
        "publisher": "O'Reilly Media",
        "editions": [
            {
                "year": "2016",
                "covercolour": "yellow"
            },
            {
                "year": "2011",
                "covercolour": "black"
            }
        ]
    }
];


function createBook(bookid) {
    var bookbox = document.getElementById(bookid);

    article = document.createElement("article");
    bookbox.appendChild(article);

    sectionInfo = createSection("List of Books");
    article.appendChild(sectionInfo);

    var infoTable = createTableBook(book);
    sectionInfo.appendChild(infoTable);
}

function createTableBook() {
    var table = document.createElement("table");
    table.classList.add("tableBasic");
    var tr = document.createElement("tr");
    var th1 = document.createElement("th");
    var th2 = document.createElement("th");
    var th3 = document.createElement("th");
    var th4 = document.createElement("th");
    var th5 = document.createElement("th");
    var th6 = document.createElement("th");
    th1.textContent = "Author";
    th2.textContent = "Title";
    th3.textContent = "ISBN";
    th4.textContent = "Publisher";
    th5.textContent = "Editions";
    th6.textContent = "Showcase";
    tr.appendChild(th1);
    tr.appendChild(th2);
    tr.appendChild(th3);
    tr.appendChild(th4);
    tr.appendChild(th5);
    tr.appendChild(th6);
    table.appendChild(tr);

    for (var index in books) {
        var tr = document.createElement("tr");
        var td1 = document.createElement("td");
        td1.textContent = books[index].author;
        td1.classList.add("author");
        tr.appendChild(td1);

        var td2 = document.createElement("td");
        td2.textContent = books[index].title;
        td2.classList.add("title");
        tr.appendChild(td2);

        var td3 = document.createElement("td");
        td3.textContent = books[index].isbn;
        td3.classList.add("isbn");
        tr.appendChild(td3);

        var td4 = document.createElement("td");
        td4.textContent = books[index].publisher;
        td4.classList.add("publisher");
        tr.appendChild(td4);

        var td5 = document.createElement("td");
        //td5.textContent = createEditionDetails(index);
        td5.appendChild(createEditionDetails(index));
        td5.classList.add("edition");
        tr.appendChild(td5);

        var td6 = document.createElement("td");
        var btn = document.createElement("button");
        btn.id = "btn" + index;
        btn.textContent = "Add";
        td6.appendChild(btn);
        tr.appendChild(td6);

        table.appendChild(tr);
    }
    return table;
}

function createEditionDetails(index1) {
    var table = document.createElement("table");
    table.classList.add("tableDetails");
    var tr = document.createElement("tr");
    var th1 = document.createElement("th");
    var th2 = document.createElement("th");
    th1.innerHTML = "Cover";
    th2.innerHTML = "Year";
    tr.appendChild(th1);
    tr.appendChild(th2);
    table.appendChild(tr);

    for (var index in books[index1].editions) {
        var tr = document.createElement("tr");
        var td1 = document.createElement("td");
        td1.innerHTML = books[index1].editions[index].covercolour;
        tr.appendChild(td1);
        var td2 = document.createElement("td");
        td2.innerHTML = books[index1].editions[index].year;
        tr.appendChild(td2);
        table.appendChild(tr);
    }
    return table;
}

function createHeader(h, text) {
    header = document.createElement("header");
    h = document.createElement(h);
    h.innerHTML = text;
    header.appendChild(h);
    return header;
}

function createSection(title) {
    var section = document.createElement("section");
    section.appendChild(createHeader("h3", title));
    return section;
}

function createSimpleRow(header, value) {
    const urlParams = new URLSearchParams(window.location.search);
    return $("<tr></tr>").append($("<th></th>").text(header)).append($("<td></td>").text(`${urlParams.getAll(value)}`));
}

//function responsible for reading and printing out entered input from the URL of a sent contact form
function readBookOrder(booklist) {
    const $list = $(`#${booklist}`);
    const $table = $("<table>");

    $table.append(createSimpleRow("Phone", "phone"));
    $table.append(createSimpleRow("Address", "adress"));
    $table.append(createSimpleRow("Ordered books", "order"));
    $table.append(createSimpleRow("Date of delivery", "deliverydate"));
    $table.append(createSimpleRow("Method of delivery", "deliverymethod"));

    //different method of achieving similar results but needs additional work with headers
    // ['phone', 'adress', 'order', 'deliverydate', 'deliverymethod'].forEach(e => $table.append(createSimpleRow(e, e)) );

    $list.append($table);
    return $list;
}

// old attempt that was too pure JS heavy
// function readBookOrder(booklist) {
//     const list = document.getElementById(booklist);
//     const table = document.createElement("table");
//
//     const tr = document.createElement("tr");
//     const td1 = document.createElement("td");
//     td1.innerHTML = `${urlParams.get("phone")}`;
//     tr.appendChild(td1);
//     const td2 = document.createElement("td");
//     td2.innerHTML = `${urlParams.get("adress")}`;
//     tr.appendChild(td2);
//     const td3 = document.createElement("td");
//     td3.innerHTML = `${urlParams.getAll("order")}`;
//     tr.appendChild(td3);
//     const td4 = document.createElement("td");
//     td4.innerHTML = `${urlParams.get("deliverydate")}`;
//     tr.appendChild(td4);
//     const td5 = document.createElement("td");
//     td5.innerHTML = `${urlParams.get("deliverymethod")}`;
//     tr.appendChild(td5);
//
//     table.appendChild(tr);
//     list.appendChild(table);
//     return list;
// }

// function queryString()
// {
//     $.urlParam = function (name) {
//         var reg = new RegExp('[\?&]' + name + '=([^&#]*)', 'g'),
//             matches = [];
//
//         while ((results = reg.exec(window.location.href)) !== null) {
//             matches.push(results[1])
//         }
//
//         return matches;
//     };
//
//     console.log($.urlParam('phone'));
//     console.log($.urlParam('adress'));
//     console.log($.urlParam('order'));
//     console.log($.urlParam('deliverydate'));
//     console.log($.urlParam('deliverymethod'));
//     var printphone = $("#" + "phoneNumber").text($.urlParam('phone'));
//     var printadress = $("#" + "adress").text($.urlParam('adress'));
//     var printorder = $("#" + "order").text($.urlParam('order'));
//     var printdeliverydate = $("#" + "deliverydate").text($.urlParam('deliverydate'));
//     var printdeliverymethod = $("#" + "deliverymethod").text($.urlParam('deliverymethod'));
// }